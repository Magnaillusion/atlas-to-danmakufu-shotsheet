class AnimationFrame:
    def __init__(self, frames, x1, y1, x2, y2):
        self.frames = frames
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

    def __str__(self):
        return 'animation_data({0}, {1}, {2}, {3}, {4})'.format(self.frames, self.x1, self.y1, self.x2, self.y2)

    def __repr__(self):
        return '''Frames: {0}
            Rect: [{1}, {2}, {3}, {4}]'''.format(self.frames, self.x1, self.y1, self.x2, self.y2)
