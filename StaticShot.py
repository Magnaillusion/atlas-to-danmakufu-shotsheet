from Shot import Shot
from Color import Color


class StaticShot(Shot):
    def __init__(self, index, render='ALPHA', collision: int = -1, delay_color=Color(), rect=[0, 0, 0, 0],
                 fixed_angle=True, angular_velocity=0, name="DUMMYDUMMY"):
        self.index = index
        self.render = render
        self.collision = collision
        self.delay_color = delay_color
        self.fixed_angle = fixed_angle
        self.angular_velocity = angular_velocity
        self.rect = rect
        self.name = name

    def __str__(self):
        reply = "ShotData{{id = {0}  ".format(self.index)

        reply += "rect = ({0}, {1}, {2}, {3}) ".format(self.rect[0], self.rect[1], self.rect[2], self.rect[3])

        if self.render != 'ALPHA':
            reply += "render = {0} ".format(self.render)

        if self.collision >= 0:
            reply += "collision = {0} ".format(self.collision)

        if self.fixed_angle:
            reply += "fixed_angle = true angular_velocity = {0} }}".format(self.angular_velocity)

        delay_string = str(self.delay_color)
        reply += "delay_color = {0} ".format(delay_string)

        return reply

    def __repr__(self):
        return '''Id: {0}
        Render Type: {1}
        Collision: {2}
        Delay Color: {3}
        Rect in atlas: {4}
        Is Fixed Angle?: {5}
        Angular Velocity: {6}'''.format(self.index, self.render, self.collision, str(self.delay_color),
                                        self.fixed_angle, self.angular_velocity, str(self.rect))
