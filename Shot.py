from Color import Color


class Shot:
    def __init__(self, index, render='ALPHA', collision: int = 5, delay_color=Color(), fixed_angle=True,
                 angular_velocity=0, name="DUMMYDUMMY"):
        # Type assertions
        assert isinstance(index, int)

        # Declarations
        self.index = index
        self.render = render
        self.collision = collision
        self.delay_color = delay_color
        self.fixed_angle = fixed_angle
        self.angular_velocity = angular_velocity
        self.name = name

    def __cmp__(self, other):
        return self.name == other.name
