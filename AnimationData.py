from AnimationFrame import AnimationFrame


class AnimationData:
    def __init__(self):
        self.__animation_frames = []

    def __str__(self):
        reply = 'AnimationData\x7B'

        for frame in self.__animation_frames:
            reply += str(frame) + " "

        reply += "\x7D"

        return reply

    def __repr__(self):
        reply = "Animation Data:\n"

        for frame in self.__animation_frames:
            reply += repr(frame) + '\n'

        return reply

    def add_frame(self, frame, x1, y1, x2, y2):
        self.__animation_frames.append(AnimationFrame(frame, x1, y1, x2, y2))
