from StaticShot import StaticShot
from AnimatedShot import AnimatedShot
from AnimationData import AnimationData
from Color import Color
from Shotsheet import Shotsheet
import xml.dom.minidom
from xml.dom.minidom import parse


def parsexmlshotsheet(path, output_file=''):
    domtree = xml.dom.minidom.parse(path)
    atlas = domtree.documentElement

    shot_array = []
    index = 0

    static_sprite_list = atlas.getElementsByTagName('sprite')
    for sprite in static_sprite_list:

        name = sprite.getAttribute('n')
        x = int(sprite.getAttribute('x'))
        y = int(sprite.getAttribute('y'))
        w = int(sprite.getAttribute('w'))
        h = int(sprite.getAttribute('h'))
        render = 'ALPHA'
        delay_r = 255
        delay_g = 255
        delay_b = 255
        fixed_angle = False
        angular_velocity = 1
        collision = -1

        if sprite.hasAttribute('render'):
            render = sprite.getAttribute('render')

        if sprite.hasAttribute('delay_r'):
            delay_r = int(sprite.getAttribute('delay_r'))

        if sprite.hasAttribute('delay_g'):
            delay_g = int(sprite.getAttribute('delay_g'))

        if sprite.hasAttribute('delay_b'):
            delay_b = int(sprite.getAttribute('delay_b'))

        if sprite.hasAttribute('fixed_angle'):
            if sprite.getAttribute('fixed_angle') == 'true':
                fixed_angle = True

            else:
                fixed_angle = False

        if sprite.hasAttribute('angular_velocity'):
            angular_velocity = int(sprite.getAttribute('angular_velocity'))

        if sprite.hasAttribute('collision'):
            collision = sprite.getAttribute('collision')

        delay_color = Color(delay_r, delay_g, delay_b)
        sprite_rect = [x, y, x + w, y + h]

        shot = StaticShot(index, render, collision, delay_color, sprite_rect, fixed_angle, angular_velocity, name)
        shot_array.append(shot)
        index += 1

    animated_sprite_list = atlas.getElementsByTagName('animation')
    for sprite in animated_sprite_list:

        name = sprite.getAttribute('n')
        render = 'ALPHA'
        delay_r = 255
        delay_g = 255
        delay_b = 255
        fixed_angle = False
        angular_velocity = 1
        collision = -1

        if sprite.hasAttribute('render'):
            render = sprite.getAttribute('render')

        if sprite.hasAttribute('delay_r'):
            delay_r = int(sprite.getAttribute('delay_r'))

        if sprite.hasAttribute('delay_g'):
            delay_g = int(sprite.getAttribute('delay_g'))

        if sprite.hasAttribute('delay_b'):
            delay_b = int(sprite.getAttribute('delay_b'))

        if sprite.hasAttribute('fixed_angle'):
            if sprite.getAttribute('fixed_angle') == 'true':
                fixed_angle = True

            else:
                fixed_angle = False

        if sprite.hasAttribute('angular_velocity'):
            angular_velocity = int(sprite.getAttribute('angular_velocity'))

        if sprite.hasAttribute('collision'):
            collision = sprite.getAttribute('collision')

        animation_frame_list = sprite.getElementsByTagName('frame')
        frameData = AnimationData()

        for animation_frame in animation_frame_list:
            f = int(animation_frame.getAttribute('f'))
            x = int(animation_frame.getAttribute('x'))
            y = int(animation_frame.getAttribute('y'))
            w = int(animation_frame.getAttribute('w'))
            h = int(animation_frame.getAttribute('h'))

            frameData.add_frame(f, x, y, x + w, y + h)

        delay_color = Color(delay_r, delay_g, delay_b)
        shot = AnimatedShot(index, render, collision, delay_color, frameData, fixed_angle, angular_velocity, name)
        shot_array.append(shot)
        index += 1

    image_path = atlas.getAttribute('imagePath')
    delay_info = atlas.getElementsByTagName('delay')[0]
    delay_x1 = int(delay_info.getAttribute('x'))
    delay_y1 = int(delay_info.getAttribute('y'))
    delay_x2 = delay_x1 + int(delay_info.getAttribute('w'))
    delay_y2 = delay_y1 + int(delay_info.getAttribute('h'))

    delay_rect = [delay_x1, delay_y1, delay_x2, delay_y2]

    reply = Shotsheet(image_path, delay_rect, shot_array, output_file)

    return reply
