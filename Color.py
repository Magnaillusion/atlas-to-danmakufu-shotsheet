class Color:
    def __init__(self, r=0, g=0, b=0):
        self.r = r
        self.g = g
        self.b = b

    def __repr__(self):
        return '(R: ' + str(self.r) + ', G: ' + str(self.g) + ', B: ' + str(self.b) + ')'

    def __str__(self):
        return "({0}, {1}, {2})".format(self.r, self.g, self.b)
