from ShotsheetParser import parsexmlshotsheet

output_name = "bullets.dnh"
result = parsexmlshotsheet("bullets.xml", output_name)

output_file = open(output_name, 'w')

output_file.write(str(result))

output_file.close()

const_name = "bullets_const.dnh"
const_file = open(const_name, 'w')

const_file.write(result.generate_const_file())

const_file.close()
