from StaticShot import StaticShot
from AnimatedShot import AnimatedShot


class Shotsheet:
    def __init__(self, shot_image, delay_rect=[0, 0, 0, 0], shot_array=[], output_file='output.dnh'):
        assert isinstance(shot_image, str)

        self.shot_image = shot_image
        self.delay_rect = delay_rect
        self.shot_array = shot_array
        self.output_file = output_file

    def __str__(self):
        reply = "#UserShotData\n\nshot_image = {0}\ndelay_rect=({1}, {2}, {3}, {4})\n\n".format(self.shot_image,
                                                                                                self.delay_rect[0],
                                                                                                self.delay_rect[1],
                                                                                                self.delay_rect[2],
                                                                                                self.delay_rect[3])

        for shot in self.shot_array:
            reply += str(shot) + "\n"

        return reply

    def __repr__(self):
        return str(self)

    def add_shot(self, shot):
        assert isinstance(shot, StaticShot) or isinstance(shot, AnimatedShot)
        self.shot_array.append(shot)

    def generate_const_file(self):
        reply = 'local\n\x7B\n    let CSD = GetCurrentScriptDirectory;\n    let path = CSD ~ "'
        reply += self.output_file
        reply += '";\n    LoadEnemyShotData(path);\n\x7D\n\n'

        for shot in self.shot_array:
            reply += 'let '
            reply += shot.name
            reply += ' = '
            reply += str(shot.index)
            reply += ';\n'

        return reply
