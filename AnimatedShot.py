from Shot import Shot
from Color import Color
from AnimationData import AnimationData


class AnimatedShot(Shot):
    def __init__(self, index, render='ALPHA', collision: int = -1, delay_color=Color(), animation_data=AnimationData(),
                 fixed_angle=True, angular_velocity=0, name="DUMMYDUMMY"):
        self.index = index
        self.render = render
        self.collision = collision
        self.delay_color = delay_color
        self.fixed_angle = fixed_angle
        self.angular_velocity = angular_velocity
        self.animation_data = animation_data
        self.name = name

    def __str__(self):
        reply = "ShotData{{id = {0} ".format(self.index)

        if self.render != 'ALPHA':
            reply += "render = {0} ".format(self.render)

        if self.collision >= 0:
            reply += "collision = {0} ".format(self.collision)

        reply += "delay_color = {0} ".format(str(self.delay_color))

        if self.fixed_angle:
            reply += "fixed_angle = true angular_velocity = {0} ".format(self.angular_velocity)

        reply += "{0} }}".format(str(self.animation_data))

        return reply

    def add_animation_frame(self, frame, x1, y1, x2, y2):
        self.animation_data.add_frame(frame, x1, y1, x2, y2)

